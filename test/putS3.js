const assert = require('assert');
const { mockClient } = require('aws-sdk-client-mock');
const { S3Client, PutObjectCommand } = require('@aws-sdk/client-s3');
const { main } = require('../putS3');
const config = require('../config');



async function assertFailedExecution(message, messageBody, config){
  try {
    await main(config, messageBody);
  } catch(err){
    return err.message.should.match(message)
  }
  throw new Error('should have errored')
}

const s3ClientMock = mockClient(S3Client);

function injectS3PutObject(data) {
  if (data instanceof Error) {
    return s3ClientMock.on(PutObjectCommand).rejects(data);
  }
  return s3ClientMock.on(PutObjectCommand).resolves(data);
}

function clone(obj) {
  return JSON.parse(JSON.stringify(obj))
}


describe('Sending via S3', () => {
  beforeEach(() => {
    s3ClientMock.reset();
  });

  describe('S3 Upload', () => {

    it('should upload to S3 successfully', async () => {
      const dataToSend = 'Text to send'
      injectS3PutObject(dataToSend);
      const expectedResponse = 'Text to send'
      const outcome = await main(config, null);
      assert.strictEqual(outcome, expectedResponse);
    });

    it('should fail if the config is missing bucket name', async () => {
      testconfig = clone(config);
      delete testconfig.bucketName
      await assertFailedExecution('Missing bucket name','dd', testconfig)
    });

    it('should fail if the config is missing key', async () => {
      testconfig = clone(config);
      delete testconfig.bucketKey
      await assertFailedExecution('Missing bucket key','dd', testconfig)
    });

    it('should fail if the config is missing region', async () => {
      testconfig = clone(config);
      delete testconfig.region
      await assertFailedExecution('Missing region','dd', testconfig)
    });

    it('should fail if the config is missing access key', async () => {
      testconfig = clone(config);
      delete testconfig.access
      await assertFailedExecution('Missing access key','dd', testconfig)
    });

    it('should fail if the config is missing secret', async () => {
      testconfig = clone(config);
      delete testconfig.secret
      await assertFailedExecution('Missing secret','dd', testconfig)
    });
  });
});