const { PutObjectCommand, S3Client } = require('@aws-sdk/client-s3');
const config = require('./config');

function validateS3Config(config) {
  if (!config) throw new Error('Missing config');
  if (!config.bucketName) throw new Error('Missing bucket name');
  if (!config.bucketKey) throw new Error('Missing bucket key');
  if (!config.region) throw new Error('Missing region');
  if (!config.access) throw new Error('Missing access key');
  if (!config.secret) throw new Error('Missing secret');

  return config;
}

const client = new S3Client({
  region: config.region,
  credentials: {
    accessKeyId: config.access,
    secretAccessKey: config.secret,
  },
});

async function main(config, messageBody) {
  validateS3Config(config);
  const command = new PutObjectCommand({
    Bucket: config.bucketName,
    Key: config.bucketKey,
    Body: messageBody,
  });

  const response = await client.send(command);
  return response;
}

module.exports = { main };
